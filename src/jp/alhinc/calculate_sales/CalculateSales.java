package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap Long型：ラッパークラス
		Map<String, Long> branchSales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1)

		//売上ファイル読み込み処理
		File[] files = new File(args[0]).listFiles();

		//List(ArrayList)の作成(Listの使い方を学ばせたい)
		//List,ArrayListを使用するにはインポートが必要
		List<File> rcdFiles = new ArrayList<>();

		//ファイル名の取得、ファイルの数だけ繰り返す
		for(int i = 0; i < files.length ; i++) {
			String fileName = files[i].getName();

			//ファイル、売上ファイル名のチェック
			if(fileName.matches("^[0-9]{8}.rcd$")) {
				//読み込み用のファイルパスをListに格納
				rcdFiles.add(files[i]);
			}
		}

		// ※ここから集計処理を作成してください。(処理内容2-2)

		BufferedReader br = null;

		//条件に一致したファイルの数だけ繰り返す
		for(int i = 0; i < rcdFiles.size(); i++) {

			try {

				//ファイルパスをBufferedReaderに渡す
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				//ファイルの中身を記載しておくArrayListを作成する
				ArrayList<String> fileContents = new ArrayList<>();

				String line = "";
				//lineに1行ずつ読み込んだ値を代入し、nullじゃないならtrue
				while((line = br.readLine()) != null) {

					//ArrayListに追加する。
					fileContents.add(line);
				}

				//支店コードを読み込む
				String branchCode = fileContents.get(0);

				//売上金額の加算
				//読み込んだ金額をLong型に変換する
				long fileSale = Long.parseLong(fileContents.get(1));

				//branchSalesに格納されている値を読み込んで、今回読み込んだものを加算する
				//branchSalesがLong型のMapの為、大文字のLong
				Long saleAmount = branchSales.get(branchCode) + fileSale;

				//加算したものをbranchSalesに格納しなおす
				branchSales.put(branchCode, saleAmount);
				//System.out.println( branchSales.get(branchCode));



			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}


		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {

		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {

				//読み込んだ値をカンマで区切る、items[0]:支店コード　items[1]：支店名
				String[] items = line.split(",");

				//マップに支店コードをkey , 支店名をvalueとして格納していく。
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);



			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {

		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		//BufferedReader、FileReaderと書き方は同じ
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			bw = new BufferedWriter(new FileWriter(file));


			//拡張For文：指定した配列やマップなどに格納されている値を取り出して使用する
			//keySetメソッドを使って、支店コードを取り出し、その数だけ繰り返し
			for(String key : branchNames.keySet()) {

				//書き込み処理
				//支店番号,支店名,金額（keyを指定してMapから取りだしている）
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));

				//改行
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
